# stretch2x_hls 

Related to the master's thesis "Heterogeneous hw / sw acceleration of a video game in a reconfigurable MPSoC" carried out at the UPM University in 2018. This is a Vivado HLS project source code to create the hardware implementation of the "stretch2x" function from the Crispy-DOOM source code. Its functionality is to read an input 640x400 frame and create an output 1280x960 frame.  

## Getting Started

You have to include this files in a Vivado HLS project.


## Authors

* **David Lima** - *davidlimaastor@gmail.com* - [BitBucket](https://bitbucket.org/d_lima)

## License

No license, feel free to use as an example.

