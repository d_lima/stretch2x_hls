/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	Stretch2x Hardware Accelerator project
 * 		File: - "stretch2x_hw.cpp"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *
 *---------------------------------------------------------------------------*/
#include "stretch2x_hw.h"


/*---------------------------------------------------------------------------*/
void stretch2x_hw(byte src[SRC_RANGE], byte dest[DEST_RANGE], byte fragments)
{
#pragma HLS INTERFACE axis register both port=dest
#pragma HLS INTERFACE axis register both port=src
    int y, x, z;

    byte temp_value;

    // byte line_buffer[LINES][SCREENWIDTH];
    byte line_buffer[SCREENWIDTH];
#pragma HLS ARRAY_PARTITION variable=line_buffer block factor=2 dim=1

    /* For every 5 lines of src_buffer, 12 lines are written to dest_buffer */
    // (200 -> 960)
    for_height:for (y=0; y<(SCREENHEIGHT/fragments); y += 5)
    {
        /* Store the data in line 0 */
        src_line0:for (x=0; x<SCREENWIDTH; x++)
        {
#pragma HLS UNROLL factor=2
            line_buffer[x] = *src;
            src++;
        }

        /* Write 5 times the line */
        line_0: for (z=0; z<3; z++)
	    {
	        write_line0:for (x=0; x<SCREENWIDTH; x++)
	        {
	        	temp_value = line_buffer[x];
				*dest = temp_value;
				dest++;
				*dest = temp_value;
				dest++;
	        }
	    } // line_0


        /* Store the data in line 1 */
        src_line1:for (x=0; x<SCREENWIDTH; x++)
        {
#pragma HLS UNROLL factor=2
            line_buffer[x] = *src;
            src++;
        }

        /* Write 5 times line 1 */
        line_1:for (z=0; z<3; z++)
        {
        	write_line1:for (x=0; x<SCREENWIDTH; x++)
	        {
            	temp_value = line_buffer[x];
				*dest = temp_value;
				dest++;
				*dest = temp_value;
				dest++;
	        }
        } // line 1


        /* Store the data in line 2 */
        src_line2:for (x=0; x<SCREENWIDTH; x++)
	    {
#pragma HLS UNROLL factor=2
            line_buffer[x] = *src;
            src++;
	    }

        /* Write 5 times line 2 */
	    line_2:for (z=0; z<2; z++)
	    {
	        write_line2:for (x=0; x<SCREENWIDTH; x++)
	        {
            	temp_value = line_buffer[x];
				*dest = temp_value;
				dest++;
				*dest = temp_value;
				dest++;
	        }
	    } // line 2


        /* Store the data in line 3 */
	    src_line3:for (x=0; x<SCREENWIDTH; x++)
	    {
#pragma HLS UNROLL factor=2
            line_buffer[x] = *src;
            src++;
	    }

        /* Write 5 times line 3 */
	    line_3:for (z=0; z<2; z++)
	    {
	        write_line3:for (x=0; x<SCREENWIDTH; x++)
            {
            	temp_value = line_buffer[x];
				*dest = temp_value;
				dest++;
				*dest = temp_value;
				dest++;
	        }
	    } // line_3


        /* Store the data in line 4 */
	    src_line4:for (x=0; x<SCREENWIDTH; x++)
	    {
#pragma HLS UNROLL factor=2
            line_buffer[x] = *src;
            src++;
	    } // store_line_4

        /* Write 4 times line 4 */
        line_4:for (z=0; z<2; z++)
	    {
        	write_line4:for (x=0; x<SCREENWIDTH; x++)
	        {
            	temp_value = line_buffer[x];
				*dest = temp_value;
				dest++;
				*dest = temp_value;
				dest++;
	        }

         } // line_4

    } // loop_height
}

