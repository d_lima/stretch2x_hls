/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	Stretch2x hardware Accelerator project
 * 		File: - "stretch2x_tb.cpp"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *     [12-11-2018]: Hardware module changed to work with arrays of structures.
 *
 *---------------------------------------------------------------------------*/
#include "stretch2x_tb.h"

using namespace std;


byte* matToBytes(cv::Mat image)
{
    int size = image.total() * image.channels();
    byte * bytes = new byte[size];

    std::memcpy(bytes,image.data,size * sizeof(byte));

    return bytes;
}


cv::Mat bytesToMat(byte* bytes,int width,int height)
{
     cv::Mat image = cv::Mat(height, width, CV_8UC3, bytes).clone();

     return image;
}

void saveInput(cv::Mat img)
{
    byte *input_data;
    FILE *save_input;

    unsigned char file_data[INPUT_WIDTH*INPUT_HEIGHT*3];

    input_data = matToBytes(img);

    char buffer[50];
    snprintf(buffer, 50*sizeof(char), "input_%u_%u.txt", INPUT_WIDTH, INPUT_HEIGHT);
    save_input = fopen(buffer, "w");

    for (int h=0; h<(INPUT_WIDTH*INPUT_HEIGHT*3); h++)
    {
	    file_data[h] = *input_data;
	    input_data++;
    }

    fwrite(file_data, 1, sizeof(file_data), save_input);
    fclose(save_input);
}


/*------------------------------- Main --------------------------------------*/
int main(int argc, char** argv )
{
    /* Errors counter */
    int c_errors = 0;

    /* Pointers to images*/
    byte *check_golden;		// Check golden copy
    byte *check_out_sw;		// check software output
    byte *check_out_hw; 	// check hardware output

    byte *bytes_img_in;

    /* Read the input and golden images */
    char buffer[50];
    snprintf(buffer, 50*sizeof(char), "input_%u_%u.PPM", INPUT_WIDTH, INPUT_HEIGHT);
    cv::Mat img_in = cv::imread(buffer,cv::IMREAD_COLOR);
    saveInput(img_in);

    /* (RGB) output images */
    cv::Mat img_out_hw = cv::Mat(OUTPUT_WIDTH, OUTPUT_HEIGHT, CV_8UC3);
    cv::Mat img_out_sw = cv::Mat(OUTPUT_WIDTH, OUTPUT_HEIGHT, CV_8UC3);

    /* Structures used in the HW */
    byte *input;
    byte *output;

    /* Print some info about the input image */
    printf("----------------------\n");
    printf("Input image name: doom.PPM\n");
    printf("Number of channels: %d\n",img_in.channels());
    printf("Rows: %d\n",img_in.rows);
    printf("Columns: %d\n",img_in.cols);
    printf("Pixel size: %u\n",img_in.elemSize());

    input = (byte *)malloc(INPUT_WIDTH * INPUT_HEIGHT * sizeof(byte) * img_in.channels());
    if (input == NULL) { fputs("Memory error input", stderr); exit(2); }
    output = (byte *)malloc(OUTPUT_WIDTH * OUTPUT_HEIGHT * sizeof(byte) * img_in.channels());
    if (output == NULL) { fputs("Memory error output", stderr); exit(2); }

    /*----------------------------------
    ------------- Hardware -------------
    ----------------------------------*/
    printf("----------------------\n");
    printf("Executing Hardware implementation ...\n");

    input = matToBytes(img_in);

    printf("Calling the UUT ...");

    int step_height_input = INPUT_HEIGHT / FRAGMENTS;
    printf("step height input: %u \n", step_height_input);
    int step_height_output = OUTPUT_HEIGHT / FRAGMENTS;
    printf("step height output: %u \n",step_height_output);


    for (int n=0; n<FRAGMENTS; n++) {
    	printf("Launching %u \n", n);
    	stretch2x_hw(
    			input + (n * step_height_input * INPUT_WIDTH * sizeof(byte)),
    			output +(n * step_height_output * OUTPUT_WIDTH * sizeof(byte)),
				FRAGMENTS);
    }

    printf("OK\n");

    /* Store result */
    printf("Writing image to Doom_out_hw.PPM ...");
    img_out_hw = bytesToMat(output, OUTPUT_WIDTH, OUTPUT_HEIGHT);

    snprintf(buffer, 50*sizeof(char), "Doom_out_HW_%u_%u.PPM", OUTPUT_WIDTH, OUTPUT_HEIGHT);
    cv::imwrite(buffer, img_out_hw);
    printf("OK\n");


    /*----------------------------------
    ------------- Software -------------
    ----------------------------------*/
    printf("----------------------\n");
    printf("Executing Software implementation ...\n");

    /* This is needed to move the destination pointer */
    dest_pitch = PITCH;

    /* Destination has 4 times rows and 4.8 times columns */
    dest_buffer = (byte*)malloc(OUTPUT_WIDTH * OUTPUT_HEIGHT * img_in.channels() * sizeof(byte));

    /* Transform input in bytes */
    src_buffer = matToBytes(img_in);

    /* Call software */
    printf("Calling SW implementation ...");
    I_Stretch2x_SW(X1, Y1, X2, Y2);
    printf("OK\n");

    /* Transform into cv::Mat to store the result */
    img_out_sw = bytesToMat(dest_buffer, OUTPUT_WIDTH, OUTPUT_HEIGHT);

    printf("Writing image to Doom_out_sw...");
    snprintf(buffer, 50*sizeof(char), "Doom_out_SW_%u_%u.PPM", OUTPUT_WIDTH, OUTPUT_HEIGHT);
    cv::imwrite(buffer, img_out_sw);
    printf("OK\n");


    /*-------------------------------------
    --------------- Check -----------------
    -------------------------------------*/
    printf("----------------------\n");
    printf("Checking results ...\n");

    /* Take pointers to check values */
    printf("Gathering images information ...");
    check_out_hw = matToBytes(img_out_hw);
    check_out_sw = matToBytes(img_out_sw);
    printf("OK\n");

    /* Loop to check bytes */
    printf("Checking bytes in both images ... \n");

    for (int i=0; i<(OUTPUT_WIDTH*OUTPUT_HEIGHT*img_in.channels()); i++)
    {
        /* Check if values are different */
        if ((*check_out_sw != *check_out_hw) ) {

            c_errors++;

            if (c_errors<15) {
				printf("i: %u\t SW: %u, HW: %u \n",i , *check_out_sw,
					 *check_out_hw);
            }

        } // end if differences

        /* Next values */
        check_out_hw++;
        check_out_sw++;

    } // for I values
    printf("...\n");
    printf("OK\n");

    /* Free memory */
    free(dest_buffer);
    free(input);
    free(output);

    printf("Checking simulation result... \n");
    if (c_errors!=0)
    {
        fprintf(stdout, "-------------------------------------------\n");
        fprintf(stdout, "FAIL: Output DOES NOT match the golden output\n");
        fprintf(stdout, "Number of errors: %d \n", c_errors);
        fprintf(stdout, "-------------------------------------------\n");
        return 1;
    } // end if no errors
    else
    {
        fprintf(stdout, "-------------------------------------------\n");
        fprintf(stdout, "PASS: The output matches the golden output!\n");
	    fprintf(stdout, "-------------------------------------------\n");
	    return 0;
    } // end if errors

}
